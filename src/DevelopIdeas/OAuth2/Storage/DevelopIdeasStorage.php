<?php

namespace DevelopIdeas\OAuth2\Storage;

use Storm\OAuth\Storage\Storage as StormStorage;
use OAuth2\OpenID\Storage\UserClaimsInterface;
use OAuth2\OpenID\Storage\AuthorizationCodeInterface as OpenIDAuthorizationCodeInterface;
use OAuth2\Storage\Pdo;

class DevelopIdeasStorage extends Pdo {

	protected $app;
    protected $siteGlobalManager;
    protected $userManager;

    public function __construct($connection, $app, $config = array())
    {
        parent::__construct($connection, $config);
		$this->app = $app;
		if (!isset($this->app['conf']->global_org_id)) {
			$this->app['conf']->global_org_id = 0;
		}
        $this->siteGlobalManager = $app['site_global_manager'];
        $this->userManager = $app['user_manager'];
    }

    protected function checkPassword($user, $password)
    {
        //return $user['password'] == sha1($password);
		//$this->getClientDetails();
		$clientId = $this->app['oauth_server']->getClientAssertionType()->getClientId();
        return $this->userManager->loginFromOAuth($clientId, $user['username'], $password);
    }

    public function getUser($username)
    {
        $username = strtolower($username);
        $stmt = $this->db->prepare($sql = sprintf('select u.*, s.scope from user u left join oauth_user_scope s on u.id = s.user_id where organisation_id = :org_id and username = :username'));
        $stmt->execute(array('org_id' => $this->app['conf']->global_org_id, 'username' => $username));
        //if (!$userInfo = $stmt->fetch(\PDO::FETCH_BOTH)) {
        if (!$userInfo = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            return false;
        }

        // the default behavior is to use "username" as the user_id
        return array_merge(array(
            'user_id' => $userInfo['username'],
            'user_db_id' => $userInfo['id'],
        ), $userInfo);
    }

    public function setUser($username, $password, $firstName = null, $lastName = null)
    {
        $username = strtolower($username);
        $settings = User::newInstance('UserByUsername');
        $user = $this->siteGlobalManager->getItems($settings, array(':org_id' => $this->app['conf']->global_org_id, ':username' => $username));
        if (!$user) {
            $user = User::newInstance('UserByUsername');
            $user->id = 0;
            $user->organisation_id = 2;
            $user->user_group_id = 2;
            $user->login_attempts = 0;
            $skipVerification = true;
            if ($skipVerification) {
               $user->validated = 1;
               $user->validation_code = 0;
               $user->status = 'Active';
            } else {
               $user->validated = 0;
               $user->validation_code = $this->userManager->getValidationCode(5, true, $username);
               $user->status = 'Awaiting Validation Code';
            }
            $user->active = 1;
            $user->deleted = 0;
        }
        $user->username = $username;
        $user->first_name = $firstName;
        $user->last_name = $lastName;
        if (!empty($password)) {
           $user->salt = $this->userManager->generateSalt();
           $user->password = $this->userManager->hashPassword($password, $user->salt);
        }

        return $manager->createUpdateItem($user) ? true : false;
    }
}
